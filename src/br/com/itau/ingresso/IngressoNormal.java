package br.com.itau.ingresso;

public class IngressoNormal extends Ingresso {
    public IngressoNormal() {
    }

    public IngressoNormal(double valor) {
        super(valor);
    }

    public void imprimirMesagemIngressoNormal(){
        System.out.println("Ingresso Normal");
    }
}
