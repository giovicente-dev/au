package br.com.itau.ingresso;

public class IngressoApp {
    public static void main(String[] args) {
        IngressoNormal normal = new IngressoNormal(500);

        normal.imprimirMesagemIngressoNormal();
        System.out.println("Valor do ingresso normal: " + normal.getValor());

        IngressoVip vip = new IngressoVip(500.00, 180.00);
        System.out.println("Valor do ingresso VIP: " + vip.getValorVip(vip.getValor()));
    }
}
