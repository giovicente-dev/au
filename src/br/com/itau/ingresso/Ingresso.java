package br.com.itau.ingresso;

public class Ingresso {
    private double valor;

    public Ingresso(){}

    public Ingresso(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return valor;
    }

    public void imprimirValor(){
        System.out.println("O valor do ingresso é " + valor);
    }
}
