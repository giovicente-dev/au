package br.com.itau.ingresso;

public class IngressoVip extends Ingresso {
    private double valorAdicional;

    public IngressoVip(double valorAdicional) {
        this.valorAdicional = valorAdicional;
    }

    public IngressoVip(double valor, double valorAdicional) {
        super(valor);
        this.valorAdicional = valorAdicional;
    }

    public double getValorVip(double valor){
        double valorVip = valor + valorAdicional;
        return valorVip;
    }
}