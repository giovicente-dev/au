package br.com.itau.funcionario;

public class Funcionario extends Pessoa {
    private double bonus;

    public Funcionario(String nome, double salario, double bonus) {
        super(nome, salario);
        this.bonus = bonus;
    }

    @Override
    public double calculaSalario(double salario) {
        double salarioAtualizado = (salario + (salario * 0.1)) + (salario * bonus);
        return salarioAtualizado;
    }
}
