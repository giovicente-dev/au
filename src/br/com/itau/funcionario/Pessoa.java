package br.com.itau.funcionario;

public class Pessoa {
    private String nome;
    private double salario;

    public Pessoa(){}

    public Pessoa(String nome, double salario){
        this.nome = nome;
        this.salario = salario;
    }

    public String getNome(){
        return nome;
    }

    public double getSalario(){
        return salario;
    }

    public double calculaSalario(double salario){
        double salarioAtualizado = salario + (salario * 0.1);
        return salarioAtualizado;
    }
}
