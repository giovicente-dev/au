package br.com.itau.funcionario;

public class Main {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa("Lucas", 15000.00);
        System.out.println("Salário da pessoa após cálculo: " + pessoa.calculaSalario(pessoa.getSalario()));

        Funcionario funcionario = new Funcionario("Giovanni", 15000.00, 0.25);
        System.out.println("Salário do funcionário após cálculo: " + funcionario.calculaSalario(funcionario.getSalario()));
    }
}
